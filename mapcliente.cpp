#include "funcoescliente.h";

int main(void)
{
    map<int, cliente>Cliente;
    map<int, cliente>::iterator aux;
    cliente auxcliente;
    int opcao;
    string nome;
    string cpf;
    string rg; 
    char sexo; 
    string endereco;
    string bairro;
    string cidade; 
    string estado; 
    int cep;
    int telefone;
    string estado_civil;
    int codigo;
    
        do
	{
		cout<<"\nPratica Map"<<endl<<endl;
		
        cout << "\n=    (1) - Inserir - com array - se duplicar - substitui o anterior =";
        cout << "\n=    (2) - Mostrar todos                               =";
        cout << "\n=    (3) - Excluir                                     =";
        cout << "\n=    (4) - Buscar pelo campo chave c/ find             =";
        cout << "\n=    (5) - Buscar pelo campo chave - com array         =";
        cout << "\n=    (6) - Buscar pelo campo nao chave                 =";
        cout << "\n=    (7) - Inserir - com make_pair     - nao substitui =";
        cout << "\n=    (8) - Numero de elementos                         =";
        cout << "\n=    (9) - Numero de registros com a chave especificada=";
        cout << "\n=    (0) - Sair                                        ="<<endl<<endl;
		cout<<"Opcao  selecionada: ";
		cin>>opcao;
		
		switch(opcao)
		{
			case 1: cout<<"\nInforme o codigo: ";
			        cin>>codigo;
                    cout<<"\nInforme o nome: ";
			        cin>>nome;
			        cout<<"\nInforme o cpf: ";
			        cin>>cpf;
			        cout<<"\nInforme o rg: ";
			        cin>>rg;
			        cout<<"\nInforme o sexo: (M/F)";
			        cin>>sexo;
			        cout<<"\nInforme o endereco: ";
			        cin>>endereco;
			        cout<<"\nInforme o bairro: ";
			        cin>>bairro;
			        cout<<"\nInforme a cidade: ";
			        cin>>cidade;			   			        
			        cout<<"\nInforme o estado: ";
			        cin>>estado;			   			        
			        cout<<"\nInforme o CEP: ";
			        cin>>cep;			   			        
			        cout<<"\nInforme o telefone: ";
			        cin>>telefone;           
			        cout<<"\nInforme o estado civil: ";
			        cin>>estado_civil;
			        auxcliente.setnome(nome);
			        auxcliente.setcpf(cpf);
			        auxcliente.setrg(rg);
			        auxcliente.setsexo(sexo);
			        auxcliente.setendereco(endereco);
			        auxcliente.setbairro(bairro);
			        auxcliente.setcidade(cidade);
			        auxcliente.setestado(estado);
			        auxcliente.setcep(cep);
			        auxcliente.settelefone(telefone);
			        auxcliente.setestado_civil(estado_civil);
			        Cliente[codigo] = auxcliente;
			        cout<<"\nRegistro inserido com sucesso";
			        
			break;

			case 2: if(!Cliente.empty())
			         {
			             for(aux = Cliente.begin(); aux != Cliente.end(); aux++)
			             {
			                 aux->second.mostracliente();
			             }    
			         }
                     else
                     {
                         cout<<"\nNenhum Cliente cadastrado";
                     }         
			break;
			
			case 3: if(!Cliente.empty())
        			{
        			    cout<<"\nInforme o codigo a ser removido: ";
        			    cin>>codigo;
        			    aux = Cliente.find(codigo);
        			    if(aux != Cliente.end())
        			    {
        			        Cliente.erase(codigo);
        			        cout<<"\nRegistro removido com sucesso";
        			    }   
                        else
                        {
                            cout<<"\nCodigo nao encontrado.";
                        }     
        			}    
        			else
        			{
        			    cout<<"\nNao existe nenhum cliente cadastrado.";
        			} 
			break;
			
			case 4: if(!Cliente.empty())
        			{
        			    cout<<"\nInforme o codigo a ser pesquisado: ";
        			    cin>>codigo;
        			    aux = Cliente.find(codigo);
        			    if(aux != Cliente.end())
        			    {
        			        aux->second.mostracliente();
        			    }   
                        else
                        {
                            cout<<"\nCodigo nao encontrado.";
                        }     
        			}    
        			else
        			{
        			    cout<<"\nNao existe nenhum cliente cadastrado.";
        			}
			break;
			
			case 5: if(!Cliente.empty())
        			{
        			    cout<<"\nInforme o codigo a ser localizado: ";
        			    cin>>codigo;
        			    Cliente[codigo].mostracliente();
                                      			    
        			}    
        			else
        			{
        			    cout<<"\nNao existe nenhum cliente cadastrado.";
        			}
			break;

			case 6: 
			break;	
			
			case 7: cout<<"\nInforme o codigo: ";
			        cin>>codigo;
			        aux = Cliente.find(codigo);
			        if(codigo != aux->first)
			        {
                        cout<<"\nInforme o nome: ";
    			        cin>>nome;
    			        cout<<"\nInforme o cpf: ";
    			        cin>>cpf;
    			        cout<<"\nInforme o rg: ";
    			        cin>>rg;
    			        cout<<"\nInforme o sexo: (M/F)";
    			        cin>>sexo;
    			        cout<<"\nInforme o endereco: ";
    			        cin>>endereco;
    			        cout<<"\nInforme o bairro: ";
    			        cin>>bairro;
    			        cout<<"\nInforme a cidade: ";
    			        cin>>cidade;			   			        
    			        cout<<"\nInforme o estado: ";
    			        cin>>estado;			   			        
    			        cout<<"\nInforme o CEP: ";
    			        cin>>cep;			   			        
    			        cout<<"\nInforme o telefone: ";
    			        cin>>telefone;           
    			        cout<<"\nInforme o estado civil: ";
    			        cin>>estado_civil;
    			        auxcliente.setnome(nome);
    			        auxcliente.setcpf(cpf);
    			        auxcliente.setrg(rg);
    			        auxcliente.setsexo(sexo);
    			        auxcliente.setendereco(endereco);
    			        auxcliente.setbairro(bairro);
    			        auxcliente.setcidade(cidade);
    			        auxcliente.setestado(estado);
    			        auxcliente.setcep(cep);
    			        auxcliente.settelefone(telefone);
    			        auxcliente.setestado_civil(estado_civil);
    			        Cliente.insert(make_pair(codigo, auxcliente)); 
    			        cout<<"\nRegistro inserido com sucesso";
                    }    
			        else
			        {
			            cout<<"\nCodigo existente.";
			        }   			        
			break;
   	
   			case 8: 
			break;	

   			case 9: 
			break;	   		
			
			default: cout<<"opcao invalida";
			system("cls");

		}
		cout<<endl;
		system("pause");
		system("cls");
} while(opcao != 0);

return(1);
}    
