#include "funcoescliente.h"

cliente::~cliente()
{
}    

cliente::cliente()
{    
}

cliente::cliente(string nome, string cpf, string rg, char sexo, string endereco, string bairro, string cidade, string estado, int cep, int telefone, string estado_civil)
{
    this->nome = nome;
    this->cpf = cpf;
    this->rg = rg;
    this->sexo = sexo;
    this->endereco = endereco;
    this->bairro = bairro;
    this->cidade = cidade;
    this->estado = estado;
    this->cep = cep;
    this->telefone = telefone;
    this->estado_civil = estado_civil;
} 

string cliente::getnome()
{
    return nome;
}              

void cliente::setnome(string nome)
{
    this->nome = nome;
}    

string cliente::getcpf()
{
    return cpf;    
}    

void cliente::setcpf(string cpf)
{
    this->cpf = cpf;
}    

string cliente::getrg()
{
    return rg;
}    

void cliente::setrg(string rg)
{
    this->rg = rg;
}    

char cliente::getsexo()
{
    return sexo;
}    

void cliente::setsexo(char sexo)
{
    this->sexo = sexo;
}    

string cliente::getendereco()
{
    return endereco;
}    

void cliente::setendereco(string endereco)
{
    this->endereco = endereco;
}    

string cliente::getbairro()
{
    return bairro;
}    

void cliente::setbairro(string bairro)
{
    this->bairro = bairro;
}    

string cliente::getcidade()
{
    return cidade;
}    

void cliente::setcidade(string cidade)
{
    this->cidade = cidade;
}    

string cliente::getestado()
{
    return estado;
}    

void cliente::setestado(string estado)
{
    this->estado = estado;
}    

int cliente::getcep()
{
    return cep;
}    

void cliente::setcep(int cep)
{
    this->cep = cep;
}    

int cliente::gettelefone()
{
    return telefone;
}    

void cliente::settelefone(int telefone)
{
    this->telefone = telefone;
}    

string cliente::getestado_civil()
{
    return estado_civil;
}    

void cliente::setestado_civil(string estado_civil)
{
    this->estado_civil = estado_civil;
}   

void cliente::mostracliente()
{
    cout<<endl<<endl<<"Registro";
    cout<<endl<<"Nome: "<<nome;
    cout<<endl<<"CPF: "<<cpf;
    cout<<endl<<"Rg: "<<rg;
    cout<<endl<<"Sexo: "<<sexo;
    cout<<endl<<"Endereco: "<<endereco;
    cout<<endl<<"Bairro: "<<bairro;
    cout<<endl<<"Cidade: "<<cidade;
    cout<<endl<<"Estado: "<<estado;
    cout<<endl<<"Cep: "<<cep;
    cout<<endl<<"Telefone: "<<telefone;
    cout<<endl<<"estado_civil: "<<estado_civil;
}     

