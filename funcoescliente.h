#include <iostream>
#include <cstdlib>
#include <string>
#include <map>

using namespace std;

class cliente
{
    private:         
        string nome; 
        string cpf;
        string rg;
        char sexo;
        string endereco;
        string bairro;
        string cidade;
        string estado;
        int cep;
        int telefone;
        string estado_civil;
        
    public:
        cliente();
        cliente(string nome, string cpf, string rg, char sexo, string endereco, string bairro, string cidade, string estado, int cep, int telefone, string estado_civil);
        ~cliente();
        string getnome();
        void setnome(string nome);
        string getcpf();
        void setcpf(string cpf);
        string getrg();
        void setrg(string rg);
        char getsexo();
        void setsexo(char sexo);
        string getendereco();
        void setendereco(string endereco);
        string getbairro();
        void setbairro(string bairro);
        string getcidade();
        void setcidade(string cidade);
        string getestado();
        void setestado(string estado);
        int getcep();
        void setcep(int cep);
        int gettelefone();
        void settelefone(int telefone);
        string getestado_civil();
        void setestado_civil(string estado_civil);
        void mostracliente();    
        
};   
